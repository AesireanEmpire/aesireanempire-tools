import adapter from '@sveltejs/adapter-vercel';
import { vitePreprocess } from '@sveltejs/kit/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	extensions: ['.svelte'],
	preprocess: [vitePreprocess()],

	kit: {
		adapter: adapter(),
		csp: {
			directives: {
				'script-src': ['self', 'unsafe-inline', 'unsafe-eval'],
				'worker-src': ['self', 'blob:'],
				'child-src': ['self', 'blob:'],
				'frame-src': ['youtube.com', 'www.youtube.com'],
				'object-src': ['none'],
				'base-uri': ['self']
			}
		}
	}
};
export default config;
