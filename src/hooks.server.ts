import { dev } from '$app/environment';
import { env } from '$env/dynamic/private';
import prisma from '$lib/db.server';
import type { Adapter } from '@auth/core/adapters';
import { PrismaAdapter } from '@auth/prisma-adapter';
import { SvelteKitAuth } from '@auth/sveltekit';
import type { UserRole } from '@prisma/client';
import * as Sentry from '@sentry/sveltekit';
import { redirect, type Handle } from '@sveltejs/kit';
import { sequence } from '@sveltejs/kit/hooks';

import Authentik from '@auth/core/providers/authentik';

Sentry.init({
	dsn: 'https://eae9f271b7ddd412ae9cb680bb973da2@o217328.ingest.sentry.io/4506221744750592',
	tracesSampleRate: 1,
	environment: dev ? 'development' : 'production'
});

declare module '@auth/core/types' {
	interface User {
		role?: UserRole;
		id: string;
	}
	interface Session {
		user: { role?: UserRole; id: string } & DefaultSession['user'];
	}
}

const authorization: Handle = async ({ event, resolve }) => {
	if (event.route.id && event.route.id.includes('(protected)')) {
		const session = await event.locals.getSession();
		if (!session) {
			throw redirect(303, '/auth/signin');
		}
	}

	return resolve(event);
};

const securityHeaders = {
	'X-Frame-Options': 'SAMEORIGIN',
	'X-Content-Type-Options': 'nosniff',
	'Referrer-Policy': 'strict-origin-when-cross-origin',
	'Permissions-Policy': ''
};

const securityHeadersHandle: Handle = async ({ event, resolve }) => {
	const response = await resolve(event);

	Object.entries(securityHeaders).forEach(([header, value]) => {
		response.headers.set(header, value);
	});

	return response;
};

export const handle = sequence(
	Sentry.sentryHandle(),
	SvelteKitAuth({
		adapter: PrismaAdapter(prisma) as Adapter,
		providers: [
			Authentik({
				name: 'authentik',
				clientId: env.DISCORD_CLIENT_ID,
				clientSecret: env.DISCORD_CLIENT_SECRET,
				issuer: env.AUTH_ISSUER
			})
		],
		callbacks: {
			session({ session, user }) {
				if (session.user) {
					session.user.role = user.role || 'MEMBER';
					session.user.id = user.id;
				}
				return session;
			}
		}
	}),
	securityHeadersHandle,
	authorization
);
export const handleError = Sentry.handleErrorWithSentry();
