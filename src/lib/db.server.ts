import { env } from '$env/dynamic/private';
import { PrismaClient } from '@prisma/client';

export default new PrismaClient({
	datasourceUrl: env.DATABASE_URL
});
