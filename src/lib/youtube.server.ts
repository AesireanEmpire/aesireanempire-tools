import { env } from '$env/dynamic/private';
import cache from '$lib/cache.server';
import { youtube, type youtube_v3 } from '@googleapis/youtube';
import _ from 'lodash';

import xml2js from 'xml2js';

const parser = new xml2js.Parser({ explicitArray: false });

const youtubeClient = youtube({
	version: 'v3',
	auth: env.YOUTUBE_API_KEY
});

export type YoutubeChannelSearchResult = {
	channelId: string;
	title: string;
	thumbnails: youtube_v3.Schema$ThumbnailDetails;
};

export type YoutubeLatestVideosResult = {
	'yt:videoId': string;
	title: string;
	published: string;
	'media:group': {
		'media:title': string;
		'media:thumbnail': { $: { url: string; width: string; height: string } };
	};
};

export interface CachedValue<T> {
	value: T;
	updatedAt: string;
}

async function getLatestVideos(
	channelId: string
): Promise<CachedValue<YoutubeLatestVideosResult[]>> {
	const cacheKey = `yt:videos:(channel:${channelId})`;
	const cachedSearch = await cache.get<CachedValue<YoutubeLatestVideosResult[]>>(cacheKey);
	if (cachedSearch) {
		return cachedSearch;
	}

	const response = await fetch(`https://www.youtube.com/feeds/videos.xml?channel_id=${channelId}`);

	const result = (await parser.parseStringPromise(await response.text())) as {
		feed: {
			entry: YoutubeLatestVideosResult[];
		};
	};
	const entries = result.feed.entry;

	const cachedValue = {
		value: entries,
		updatedAt: new Date().toISOString()
	};

	await cache.set(cacheKey, JSON.stringify(cachedValue), { ex: 60 * 60 * 1 });

	return cachedValue;
}

export async function getLatestVideosForList(
	channelIds: string[]
): Promise<CachedValue<YoutubeLatestVideosResult[]>> {
	if (channelIds.length === 0) {
		return {
			updatedAt: new Date().toISOString(),
			value: []
		};
	}

	const latestCachedVideos = await Promise.all(
		channelIds.flatMap((channelId) => getLatestVideos(channelId))
	);

	const latestUpdateTimeStamp = _.sortBy(
		latestCachedVideos,
		(video) => -new Date(video.updatedAt)
	)[0].updatedAt;

	const latestVideos = _.flatMap(latestCachedVideos, (video) => video.value);

	const videos = _.sortBy(latestVideos, (video) => {
		return -new Date(video.published);
	});

	return {
		updatedAt: latestUpdateTimeStamp,
		value: videos
	};
}

export async function searchChannels(
	query: string
): Promise<CachedValue<YoutubeChannelSearchResult[]>> {
	const cacheKey = `yt:channels:(query:${query.toLowerCase()})`;
	const cachedSearch = await cache.get<CachedValue<YoutubeChannelSearchResult[]>>(cacheKey);
	if (cachedSearch) {
		return cachedSearch;
	}

	const {
		data: { items }
	} = await youtubeClient.search.list({
		part: ['id', 'snippet'],
		q: query,
		type: ['channel'],
		maxResults: 10
	});

	const channelResults = items?.map(({ snippet }) => {
		return {
			channelId: snippet?.channelId,
			title: snippet?.title,
			thumbnails: snippet?.thumbnails
		} as YoutubeChannelSearchResult;
	});

	const value = {
		value: channelResults || [],
		updatedAt: new Date().toISOString()
	};

	await cache.set(cacheKey, JSON.stringify(value), { ex: 60 * 60 * 24 });

	return value;
}
