import { dev } from '$app/environment';
import * as Sentry from '@sentry/sveltekit';
import { handleErrorWithSentry, Replay } from '@sentry/sveltekit';

Sentry.init({
	dsn: 'https://eae9f271b7ddd412ae9cb680bb973da2@o217328.ingest.sentry.io/4506221744750592',
	tracesSampleRate: 1.0,
	replaysSessionSampleRate: 0.1,
	replaysOnErrorSampleRate: 1.0,
	integrations: [new Replay(), new Sentry.BrowserTracing()],
	environment: dev ? 'development' : 'production'
});

export const handleError = handleErrorWithSentry();
