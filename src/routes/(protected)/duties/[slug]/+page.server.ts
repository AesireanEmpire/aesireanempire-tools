import prisma from '$lib/db.server';
import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ params }) => {
	const id = params.slug;

	const duty = await prisma.duty.findFirst({
		where: {
			id: id
		},
		include: {
			information: true,
			videos: true,
			users: true
		}
	});

	if (!duty) {
		throw error(404, 'Not found');
	}

	return {
		duty: duty
	};
};

export const actions = {
	addVideo: async ({ locals, request }) => {
		const session = await locals.getSession();

		if (session?.user.role !== 'ADMIN') {
			throw error(403, 'Forbidden');
		}

		const { duty, link } = await request.json();

		await prisma.video.create({
			data: {
				url: link,
				dutyId: duty
			}
		});
	}
};
