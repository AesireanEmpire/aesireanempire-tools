import prisma from '$lib/db.server';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ url }) => {
	const limit = Number(url.searchParams.get('limit')) || 10;
	const page = Number(url.searchParams.get('page')) || 0;

	return {
		limit: limit,
		page: page,
		total: prisma.duty.count({}),
		duties: prisma.duty.findMany({
			take: limit,
			skip: page * limit
		})
	};
};
