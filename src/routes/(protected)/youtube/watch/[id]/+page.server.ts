import prisma from '$lib/db.server';
import type { Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ params, locals }) => {
	const session = await locals.getSession();
	const userId = session?.user.id;

	const watchedVideo = await prisma.youtubeWatchedVideo.findFirst({
		where: {
			videoId: params.id,
			userId: userId
		}
	});

	return { videoUrl: params.id, watched: !!watchedVideo };
};

export const actions: Actions = {
	default: async ({ params, locals }) => {
		const session = await locals.getSession();
		const userId = session?.user.id;

		const watchedVideo = await prisma.youtubeWatchedVideo.findFirst({
			where: {
				videoId: params.id,
				userId: userId
			}
		});

		if (watchedVideo) {
			await prisma.youtubeWatchedVideo.delete({
				where: {
					id: watchedVideo.id
				}
			});
		} else {
			await prisma.youtubeWatchedVideo.create({
				data: {
					videoId: params.id!,
					userId: userId
				}
			});
		}
	}
};
