import prisma from '$lib/db.server';
import { fail, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async () => {
	const channels = await prisma.youtubeChannelList.findMany({
		orderBy: {
			name: 'asc'
		},
		include: {
			channels: true
		}
	});

	return {
		channels
	};
};

export const actions: Actions = {
	createList: async ({ request }) => {
		const data = await request.json();

		try {
			await prisma.youtubeChannelList.create({
				data: {
					name: data.name
				}
			});
			return { success: true };
		} catch (error) {
			return fail(500, { message: 'Error creating channel list' });
		}
	}
};
