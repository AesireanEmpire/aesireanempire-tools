import prisma from '$lib/db.server';
import { getLatestVideosForList } from '$lib/youtube.server';
import { redirect } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ params, locals }) => {
	const session = await locals.getSession();
	const userId = session?.user.id;

	const list = await prisma.youtubeChannelList.findFirst({
		where: {
			id: params.id
		},
		include: {
			channels: true
		}
	});

	if (list) {
		const videos = await getLatestVideosForList(list?.channels.map((t) => t.youtubeId));
		const watchedList = (
			await prisma.youtubeWatchedVideo.findMany({
				where: {
					userId: userId,
					videoId: {
						in: videos.value.map((video) => video['yt:videoId'])
					}
				},
				select: {
					videoId: true
				}
			})
		).map((list) => list.videoId);

		return { list, videos, watchedList };
	} else {
		throw redirect(307, '/youtube');
	}
};
