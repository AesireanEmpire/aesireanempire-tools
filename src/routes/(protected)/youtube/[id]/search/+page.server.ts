import prisma from '$lib/db.server';
import {
	searchChannels,
	type CachedValue,
	type YoutubeChannelSearchResult
} from '$lib/youtube.server';
import { fail, redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({
	url,
	params,
	locals
}): Promise<{
	channels: CachedValue<YoutubeChannelSearchResult[]>;
	currentChannelsInList: string[];
}> => {
	const session = await locals.getSession();

	if (session?.user.role !== 'ADMIN') {
		throw redirect(307, `/youtube/${params.id}`);
	}

	const { query } = Object.fromEntries(url.searchParams);

	if (query) {
		const currentChannelsInList = await prisma.youtubeChannel.findMany({
			where: {
				youtubeChannelListId: params.id
			},
			select: {
				youtubeId: true
			}
		});

		const channels = await searchChannels(query.trim());
		return {
			channels: channels,
			currentChannelsInList: currentChannelsInList.map((list) => list.youtubeId)
		};
	}

	return {
		channels: { updatedAt: new Date().toISOString(), value: [] },
		currentChannelsInList: []
	};
};

export const actions: Actions = {
	addToList: async ({ request, params }) => {
		const { channel } = (await request.json()) as { channel: YoutubeChannelSearchResult };

		const currentChannel = await prisma.youtubeChannel.findFirst({
			where: {
				youtubeChannelListId: params.id,
				youtubeId: channel.channelId
			}
		});

		if (currentChannel) {
			return { success: true };
		}

		try {
			await prisma.youtubeChannel.create({
				data: {
					name: channel.title,
					youtubeId: channel.channelId,
					youtubeChannelListId: params.id
				}
			});

			return { success: true };
		} catch (error) {
			return fail(500, { message: 'Failed to add channel to list' });
		}
	}
};
