import { error, json, type RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async (event) => {
	const session = await event.locals.getSession();
	if (!session) throw error(401, 'Unauthorized - must be logged-in.');

	return json({
		message: 'Hello There!'
	});
};
